package lsim.az.handler;


import lsim.az.exception.NotFoundException;
import lsim.az.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ErrorResponse handleException(NotFoundException ex) {
        return new ErrorResponse(ex.getMessage(),
                ex.getCode(),
                HttpStatus.NOT_FOUND.value());
    }


}
