package lsim.az;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LsimApplication {

    public static void main(String[] args) {
        SpringApplication.run(LsimApplication.class, args);
    }

}
