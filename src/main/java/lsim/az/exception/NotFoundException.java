package lsim.az.exception;

import lombok.Data;

@Data
public class NotFoundException extends RuntimeException{

    private String message;
    private String code;

    public NotFoundException(String message, String code) {
        super(message);
        this.message = message;
        this.code = code;
    }

}
