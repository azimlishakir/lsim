package lsim.az.scheduler;


import lsim.az.dao.repository.LsimRepository;
import lsim.az.model.LsimStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

@Component
public class LsimScheduler {

    private LsimRepository lsimRepository;

    private static final Logger logger = LoggerFactory.getLogger(LsimScheduler.class);

    public LsimScheduler(LsimRepository lsimRepository) {
        this.lsimRepository = lsimRepository;
    }

    @Scheduled(cron = "0 0 12 * * ?")
    public void updateStatus() {
      logger.info("updateStatus start");
      lsimRepository.findByStatus(LsimStatus.CREATE).forEach(lsimEntity->{
          long days = Duration.between(lsimEntity.getDate(),LocalDateTime.now()).toDays();
          logger.info("days count -- {} for id {}", days, lsimEntity.getId());
          if (days >= 5) {
              lsimEntity.setStatus(LsimStatus.ACTIVE);
              lsimRepository.save(lsimEntity);
          }
      });

    }

}
