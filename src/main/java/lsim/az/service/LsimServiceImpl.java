package lsim.az.service;

import lsim.az.dao.entity.LsimEntity;
import lsim.az.dao.repository.LsimRepository;
import lsim.az.exception.NotFoundException;
import lsim.az.mapper.LsimMapper;
import lsim.az.model.LsimDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class LsimServiceImpl implements LsimService {

    private LsimRepository lsimRepository;

    private static final Logger logger = LoggerFactory.getLogger(LsimServiceImpl.class);
    private static final String NOT_FOUND_MESSAGE = "data could not find";
    private static final String NOT_FOUND_CODE = "lsim.data-not.found";


    public LsimServiceImpl(LsimRepository lsimRepository) {
        this.lsimRepository = lsimRepository;
    }

    @Override
    public LsimDto createData(LsimDto lsimDto) {
        logger.info("createData start");
        LsimEntity entity = lsimRepository.save(LsimMapper.INSTANCE.mapEntity(lsimDto));
        logger.info("createData end");
        return LsimMapper.INSTANCE.mapDTO(entity);
    }

    @Override
    public LsimDto getDataById(Long id) {
        logger.info("getDataById start for id {}",id);
        LsimEntity entity = lsimRepository.findById(id).
                orElseThrow(()-> new NotFoundException(NOT_FOUND_MESSAGE,NOT_FOUND_CODE));
        logger.info("getDataById end for id {}",id);
        return LsimMapper.INSTANCE.mapDTO(entity);
    }
}
