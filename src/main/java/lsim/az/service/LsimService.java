package lsim.az.service;

import lsim.az.model.LsimDto;

public interface LsimService {

     LsimDto createData(LsimDto lsimDto);

     LsimDto getDataById(Long id);

}
