package lsim.az.mapper;

import lsim.az.dao.entity.LsimEntity;
import lsim.az.model.LsimDto;
import lsim.az.model.LsimStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class LsimMapper {

    public static final LsimMapper INSTANCE = Mappers.getMapper(LsimMapper.class);

    @Mapping(target = "status",expression = "java(defaultStatus())")
    public abstract LsimEntity mapEntity(LsimDto request);

    public abstract LsimDto mapDTO(LsimEntity entity);

    protected LsimStatus defaultStatus() {
        return LsimStatus.CREATE;
    }

}
