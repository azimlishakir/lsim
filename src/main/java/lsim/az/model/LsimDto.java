package lsim.az.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LsimDto {

    private String number;
    private String title;
    private String messageBody;
    private LsimStatus status;
}
