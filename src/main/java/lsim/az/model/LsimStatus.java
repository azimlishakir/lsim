package lsim.az.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LsimStatus {
    ACTIVE(1),
    CREATE(0),
    BLOCKED(2);

    private final Integer id;
}
