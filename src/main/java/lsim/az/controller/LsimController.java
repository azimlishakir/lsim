package lsim.az.controller;

import lsim.az.model.LsimDto;
import lsim.az.service.LsimService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("lsim")
public class LsimController {

    private LsimService lsimService;

    public LsimController(LsimService lsimService) {
        this.lsimService = lsimService;
    }

    @GetMapping("/{id}")
    public LsimDto getData(@PathVariable(value = "id") Long id) {
        return lsimService.getDataById(id);
    }

    @PostMapping
    public ResponseEntity<LsimDto> createData(@RequestBody LsimDto lsimDto) {
        return ResponseEntity.ok(lsimService.createData(lsimDto));
    }

}
