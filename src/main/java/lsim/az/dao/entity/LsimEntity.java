package lsim.az.dao.entity;

import lombok.*;
import lsim.az.model.LsimStatus;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LsimEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String number;
    private String title;
    @Column(name = "message_body")
    private String messageBody;
    @CreationTimestamp
    private LocalDateTime date;
    @Enumerated(EnumType.ORDINAL)
    private LsimStatus status;
}
