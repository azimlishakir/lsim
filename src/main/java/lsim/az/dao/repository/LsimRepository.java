package lsim.az.dao.repository;

import lsim.az.dao.entity.LsimEntity;
import lsim.az.model.LsimStatus;
import lsim.az.scheduler.LsimScheduler;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LsimRepository extends JpaRepository<LsimEntity,Long> {
    List<LsimEntity> findByStatus(LsimStatus status);
}
